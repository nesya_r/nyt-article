const dotenv = require('dotenv');
dotenv.config();

const {
  DATABASE_NAME,
  DATABASE_USERNAME,
  DATABASE_PASSWORD,
  DATABASE_HOST,
  DATABASE_URL,
} = process.env;

module.exports = {
  development: {
    use_env_variable: DATABASE_URL && 'DATABASE_URL', 
    username: DATABASE_USERNAME || 'root',
    password: DATABASE_PASSWORD || null,
    database: `${DATABASE_NAME || 'nyt'}_development`,
    host: DATABASE_HOST || 'localhost',
    dialect: "postgres"
  },
  test: {
    use_env_variable: DATABASE_URL && 'DATABASE_URL',
    username: DATABASE_USERNAME || 'root',
    password: DATABASE_PASSWORD || null,
    database: `${DATABASE_NAME || 'nyt'}_test`,
    host: DATABASE_HOST || 'localhost',
    dialect: "postgres",
    logging: false
  },
  staging: {
    use_env_variable: DATABASE_URL && 'DATABASE_URL', 
    username: DATABASE_USERNAME,
    password: DATABASE_PASSWORD,
    database: `${DATABASE_NAME}_staging`,
    host: DATABASE_HOST,
    dialect: "postgres",
  },
  production: {
    use_env_variable: DATABASE_URL && 'DATABASE_URL', 
    username: DATABASE_USERNAME,
    password: DATABASE_PASSWORD,
    database: `${DATABASE_NAME}_production`,
    host: DATABASE_HOST,
    dialect: "postgres"
  }
}
