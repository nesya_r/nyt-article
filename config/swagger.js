const path = require('path');
const createDocs = require('swagger-jsdoc');
const {
  PORT = 8081
} = process.env;
const host =`localhost:${PORT}` ;

module.exports = createDocs({
  swaggerDefinition: {
    info: {
      title: 'API',
      version: '1.0.0',
      description: "Documentation for API."
    },
    host,
    basePath: '/v1'
  },
  apis: [
    path.resolve(__dirname, '../app/controllers/**/*.js')
  ]
});
