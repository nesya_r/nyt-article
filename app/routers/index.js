/*
 * Main Router,
 * Import every router that being created inside __dirname recursively.
 * */
const express = require('express');
const multer = require('multer');
const v1 = require('./v1');
const controller = require('../controllers');
const router = express.Router(); 

router.use('/v1', v1);
router.get('/', controller.home);
router.post('/webhook', multer().none(), (req, res) => {
  console.log('Payload', req.body);
  res.status(204).end();
});

module.exports = router;
