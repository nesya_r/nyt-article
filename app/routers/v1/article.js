
const express = require('express');
const { search } = require('../../controllers/v1/article');
const router = express.Router(); 

router.get('/search', search);

module.exports = router;
