
const express = require('express');
const { search } = require('../../controllers/v1/book');
const router = express.Router(); 

router.get('/search', search);

module.exports = router;
