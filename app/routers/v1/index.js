/*
 * Main Router,
 * Import every router that being created inside __dirname recursively.
 * */
const express = require('express');
const article = require('./article');
const book = require('./book');
const router = express.Router(); 

router.use('/article', article);
router.use('/book', book);

module.exports = router;
