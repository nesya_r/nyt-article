/*
 * Entrypoint,
 * Contains every declaration about server.
 * */
const express = require('express');
const logger = require('morgan');
const dotenv = require('dotenv');
const cors = require('cors');
const { NODE_ENV = 'development' } = process.env;

// Read .env file, so when we import routers, it will be loaded properly. 
dotenv.config();

// Instantiate express, and import routers. 
const app = express();
const router = require('./routers');
const { onError, on404 } = require('./controllers/exception');

// Install middleware on the server 
if (NODE_ENV !== 'test') app.use(logger('tiny'));
if (NODE_ENV !== 'production') {
  const swagger = require('swagger-ui-express');
  const documentation = require('../config/swagger');
  app.use(
    '/documentation',
    swagger.serve,
    swagger.setup(documentation, {
      customCssUrl: '/css/swagger.css',
      customJs: '/js/swagger.js'
    })
  );
}
// Activate CORS
app.use(
  express.static('public'),
  cors(),
  express.json(),
  router,
  on404,
  onError
);

module.exports = app;
