/*
 * Response Formatter,
 * Make sure you always call one of this function when presenting JSON response.
 * */
module.exports = {
  success: (message, { meta, data } = {}) => ({ status: true, message, meta, data }),
  error: (message, errors) => ({ status: false, message, errors })
};
