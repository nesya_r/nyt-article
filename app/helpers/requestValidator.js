/*
 * Request Validator,
 * Make sure you always call one of this function when you create new API definition.
 * */
const Validator = require('fastest-validator');
const { error } = require('../helpers/responseFormatter');
const validator = new Validator();

module.exports = (schema) => {
  const check = validator.compile(schema);
  return (req, res, next) => {
    const result = check(req.body);
    if (result === true) return next();
    return res.status(400).json(
      error('Bad request!', result)
    );
  };
};
