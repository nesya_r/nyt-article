/*
 * Dynamically import documentation.
 * If the environment staging, then it will import the compiled version.
 * Else, it will import the swagger-jsdoc one.
 */
const documentation = process.env.NODE_ENV === 'development' ? '../config/swagger' : '../swagger.json';

module.exports = require(documentation);
