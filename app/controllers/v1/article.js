const responseFormatter = require('../../helpers/responseFormatter');
const validate = require('../../helpers/requestValidator');
const { success, error } = responseFormatter;
const axios = require('axios');

const url = `https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=${process.env.API_KEY}`;

module.exports = {
  search: [
    validate({}),
    async (req, res) => {
      try {
        const response = await axios.get(url, {
          params: {
            fq: req.params.fq,
            sort: req.params.sort
          }
        } )
        res.status(200).json(
          success('success!', {
            data: {
              article: response.data.response.docs
            }
          })
        );
      }
      catch (error) {
        console.log(error)
      }
    }]
};
