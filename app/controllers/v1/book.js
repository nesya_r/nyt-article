const responseFormatter = require('../../helpers/responseFormatter');
const validate = require('../../helpers/requestValidator');
const { success, error } = responseFormatter;
const axios = require('axios');

const url = `https://api.nytimes.com/svc/books/v3/lists.json?api-key=${process.env.API_KEY}`;

module.exports = {
  search: [
    validate({}),
    async (req, res) => {
      try {
       if (req.params.list != null) {
        var list = req.params.list
       }
       else{
        var list = "hardcover-fiction"
       }
        const response = await axios.get(url, {
          params: {
            list: list
          }
        })
        res.status(200).json(
          success('success!', {
            data: {
              book: response.data.results
            }
          })
        );
      }
      catch (error) {
        console.log(error)
      }
    }]
};
