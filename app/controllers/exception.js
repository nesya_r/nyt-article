const { error } = require('../helpers/responseFormatter');

module.exports = {
  onError: (err, req, res, next) => {
    console.error(err.toString());
    return res.status(500).json(
      error(err.message)
    );
  },

  on404: (req, res, next) => res.status(404).json(
    error('Are you lost, mate?')
  )
};
