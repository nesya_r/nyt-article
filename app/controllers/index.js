/*
 * Index Controller,
 * Contains root endpoint handler, etc.
 * */
const { success } = require('../helpers/responseFormatter');

module.exports = {
  home: (req, res) => {
    res.status(200).json(
      success('Hello!')
    );
  }
};
